// @ts-check

'use strict';

// outline
// - lib
// - var
// - main

// lib
const puppeteer = require('puppeteer');
const { initDir, getHost } = require('./lib.js');
const fs = require('fs');
const { sep } = require('path');

// var
const url = 'https://diary.app.ssig33.com/347';
const margin = Object.freeze({
  right: '25mm',
  left: '25mm',
  top: '10mm',
  bottom: '10mm'
});
const date = new Date().toLocaleDateString();

// main
(async () => {
  // init puppeteer
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  initDir(url);

  try {
    // move
    await page.goto(url, { waitUntil: 'networkidle2' });

    // init vars
    const host = getHost(url);
    const title = await page.evaluate(() => document.title);
    const base = `pages${sep}${host}${sep}${title}${sep}`;

    // initDirではtitleのディレクトリを作成できないのでここで作ります
    fs.mkdirSync(`pages${sep}${getHost(url)}${sep}${title}`);

    // save page
    await page.screenshot({ path: `${base}thum.png` });
    await page.screenshot({ path: `${base}fullPage.png`, fullPage: true });
    await page.pdf({ path: `${base}main.pdf`, margin });
    fs.writeFileSync(`${base}meta.json`, JSON.stringify({ url, title, date }));
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
  await browser.close();
})();
